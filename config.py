import os
import subprocess
from libqtile.config import Key, Screen, Group, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook

home = os.path.expanduser("~")
mod = "mod4"


@hook.subscribe.startup_once
def startup():
    subprocess.call([f"{home}/.config/qtile/startup.sh"])


@hook.subscribe.screen_change
def randrchange(qtile, ev):
    qtile.cmd_restart()


keys = [
    Key([mod], "Return", lazy.spawn("alacritty"), desc="Launch terminal"),
    Key([mod], "d", lazy.spawn("rofi -modi drun,run -show drun"), desc="Run Launcher"),
    Key([mod], "y", lazy.spawn("flameshot gui -r"), desc="Screenshot"),
    Key([mod, "shift"], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer sset Master,0 5%-")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("axmier sset Master,0 5%+")),
    Key([mod, "shift"], 'r', lazy.restart()),
]

group_names = "123456789"

groups = [
    Group(i) for i in group_names
]

for i in group_names:
    keys.append(Key([mod], str(i), lazy.group[i].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(i))) # Send current window to another group


