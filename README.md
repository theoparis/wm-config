# My QTile Config

This is my qtile config. 

## Prerequisites

- qtile (obviously)
- dunst (a notification manager)
- `zsh` shell (for startup.sh)
- nm-applet (for wifi)
- nitrogen (wallpaper manager)

## Installation

```zsh
git clone https://gitlab.com/creepinson/wm-config ~/.config/qtile
```

## Updating

```zsh
cd ~/.config/qtile
git pull
```
